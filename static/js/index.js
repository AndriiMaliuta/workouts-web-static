import { getWorkouts } from "./workoutsService.js";

// dates
const today = JSJoda.LocalDate.now();

const month = JSJoda.LocalDate.now().month(); // {_value: 10, _name: 'OCTOBER'}

const datesDiv = document.querySelector("#dates");
const datePara = document.createElement("p");
datePara.innerHTML = `<p>Today: ${today}</p>`;

datesDiv.appendChild(datePara);

let workouts = await getWorkouts(location.href.substring(location.href.indexOf("\?")));

const lastWorkouts = workouts.slice(0, 10);

const workoutsDiv = document.querySelector("#workouts-data");

// create divs for workouts
lastWorkouts
  .filter((w) => w.creation_date !== undefined)
  .forEach((wk) => {
    const workoutDiv = document.createElement("div");
    let coloredType = "";
    switch (wk.workout_type) {
      case "BICEPS":
        coloredType = `<span class="bics">${wk.workout_type}</span>`;
        break;
      case "PECS":
        coloredType = `<span class="pecs">${wk.workout_type}</span>`;
        break;
      case "BACK":
        coloredType = `<span class="back">${wk.workout_type}</span>`;
        break;
      case "DELTS":
        coloredType = `<span class="delts">${wk.workout_type}</span>`;
        break;
      case "TRICEPS":
        coloredType = `<span class="trics">${wk.workout_type}</span>`;
        break;
      case "ABS":
        coloredType = `<span class="abs">${wk.workout_type}</span>`;
        break;
      default:
        coloredType = `<span >${wk.workout_type}</span>`;
        break;
    }
    workoutDiv.innerHTML = `<div class="workout-element">
      <p>${coloredType}</p>
      <p>${wk.workout_date}</p>
      <p>day: ${wk.day}</p>
      <p><b>comment</b>: ${wk.comments}</p>
    </div>`;

    workoutsDiv.appendChild(workoutDiv);
  });
