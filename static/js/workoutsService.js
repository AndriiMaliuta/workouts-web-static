
export async function getWorkouts(params) {
  let resp = await fetch(
    `https://us-central1-workouts-app2.cloudfunctions.net/go_gcp_cfunc_mongo_workouts${params}`
  );
  return await resp.json();
}

export async function getWorkoutsByDate(date) {
  let resp = await fetch(
    `https://us-central1-workouts-app2.cloudfunctions.net/go_gcp_cfunc_mongo_workouts?wDate=${date}`
  );
  return await resp.json();
}

export async function getWorkoutsByType(type) {
  let resp = await fetch(
    `https://us-central1-workouts-app2.cloudfunctions.net/go_gcp_cfunc_mongo_workouts?wType=${type}`
  );
  return await resp.json();
}

export async function getWorkoutsByMonth(month) {
  let resp = await fetch(
    `https://us-central1-workouts-app2.cloudfunctions.net/go_gcp_cfunc_mongo_workouts?month=${month}`
  );
  return await resp.json();
}

export async function getWorkoutsByYear(year) {
  let resp = await fetch(
    `https://us-central1-workouts-app2.cloudfunctions.net/go_gcp_cfunc_mongo_workouts?year=${year}`
  );
  return await resp.json();
}
