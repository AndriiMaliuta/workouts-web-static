// create workout
const createBtn = document.querySelector("#create-workout-button");

const today = JSJoda.LocalDate.now();
const create_date = document.querySelector("#workout_date");
create_date.value = today;

// button click
createBtn.addEventListener("click", (evt) => {
  evt.preventDefault;

  const sets = document.querySelector("#sets").value;
  const workout_date = document.querySelector("#workout_date").value;
  const workout_type =
    document.querySelector("#workout_type").selectedOptions[0].value;
  const comments = document.querySelector("#comments").value;

  // todo: debug
  console.log(workout_type);

  createWorkout({
    sets: sets,
    workout_date,
    workout_type,
    comments,
  });
});

function createWorkout(data) {
  fetch(
    `https://us-central1-workouts-app2.cloudfunctions.net/create-workout-mongo-go`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Origin: "https://workouts-web-static.vercel.app",
      },
      body: JSON.stringify({
        sets: data.sets,
        workout_date: data.workout_date,
        workout_type: data.workout_type,
        comments: data.comments,
      }),
    }
  );
}
